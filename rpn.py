# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import unittest
import operator

ARITHMETIC_OPERATIONS = {
    '+':  operator.add,
    '-':  operator.sub,
    '*':  operator.mul,
    '/':  operator.div
}


def calculate_rpn(expression=None):
    """
        calculate reverse polish notation
    """
    if not expression:
        expression = raw_input('Your expression: ')

    if not expression.strip():
        raise ValueError('Empty')

    stack = []
    for char in expression.split():
        if char in ARITHMETIC_OPERATIONS:
            try:
                b, a = stack.pop(), stack.pop()
                stack.append(ARITHMETIC_OPERATIONS[char](a, b))
            except Exception:
                raise ValueError('Something wrong')
        else:
            stack.append(int(char))

    return stack.pop()


class TestRPN(unittest.TestCase):
    def test_main_rpn(self):
        self.assertEqual(3, calculate_rpn('1 2 +'))
        self.assertEqual(4, calculate_rpn('2 2 +'))
        self.assertEqual(1, calculate_rpn('-4 5 +'))
        self.assertEqual(1, calculate_rpn('3 2 -'))
        self.assertEqual(4, calculate_rpn('3 2 1 - +'))
        self.assertEqual(2, calculate_rpn('4 2 5 * + 1 3 2 * + /'))
        self.assertEqual(14, calculate_rpn('5 1 2 + 4 * 3 - +'))
        self.assertEqual(55, calculate_rpn('5 8 3 + *'))


if __name__ == '__main__':
    unittest.main()
